package br.com.testmongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import static java.util.Arrays.asList;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.ascending;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author José
 */
public class Test {

    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient();
        MongoDatabase db = mongoClient.getDatabase("test");
//            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
//            db.getCollection("restaurants").insertOne(
//                    new Document("address",
//                            new Document()
//                                    .append("street", "2 Avenue")
//                                    .append("zipcode", "10075")
//                                    .append("building", "1480")
//                                    .append("coord", asList(-73.9557413, 40.7720266)))
//                            .append("borough", "Manhattan")
//                            .append("cuisine", "Italian")
//                            .append("grades", asList(
//                                    new Document()
//                                            .append("date", format.parse("2014-10-01T00:00:00Z"))
//                                            .append("grade", "A")
//                                            .append("score", 11),
//                                    new Document()
//                                            .append("date", format.parse("2014-01-16T00:00:00Z"))
//                                            .append("grade", "B")
//                                            .append("score", 17)))
//                            .append("name", "Vella")
//                            .append("restaurant_id", "41704620"));


//        FindIterable<Document> iterable = db.getCollection("restaurants").find();


        Map<String, Object> filtros = new HashMap<>();
        filtros.put("borough", "Manhattan");
        filtros.put("cuisine", "Italian");
        filtros.put("restaurant_id", "41704620");
        Document doc = new Document(filtros);
        FindIterable<Document> iterable = db.getCollection("restaurants").find(doc);
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document);
            }
        });
        iterable = db.getCollection("restaurants").find(eq("restaurant_id", "41704620"));
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                System.out.println(document);
            }
        });
    }
}
